const passwordIcons = document.querySelectorAll(".icon-password");
const passwordInputs = document.querySelectorAll('input[type="password"]');

passwordIcons.forEach((icon, index) => {
    icon.addEventListener("click", () => {
        const passwordInput = passwordInputs[index];
        if (passwordInput.type === "password") {
            passwordInput.type = "text";
            icon.classList.remove("fa-eye");
            icon.classList.add("fa-eye-slash");
        } else {
            passwordInput.type = "password";
            icon.classList.remove("fa-eye-slash");
            icon.classList.add("fa-eye");
        }
    });
});

document.querySelector("form").addEventListener("submit", (e) => {
    e.preventDefault();
    const password1 = passwordInputs[0].value;
    const password2 = passwordInputs[1].value;

    if (password1 === password2) {
        alert("You are welcome.");
    } else {
        const errorMsg = document.createElement("p");
        errorMsg.textContent = "Потрібно ввести однакові значення, epta";
        errorMsg.style.color = "red";
        const passwordForm = document.querySelector(".password-form");
        passwordForm.appendChild(errorMsg);
    }
});
